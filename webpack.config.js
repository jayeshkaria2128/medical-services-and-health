const postCSSPlugins = [
    require("postcss-import"),
    require("postcss-mixins"),
    require('postcss-simple-vars'),
    require('postcss-nested'),
    require('autoprefixer')
];
const path = require("path");
module.exports = {
    entry: './app/assets/scripts/app.js',
    output: {
        filename: "app.bundled.js",
        path: path.resolve(__dirname, "app"),
    },

    devServer: {
        before: function(app, server) {
            server.watch('./app/**/*.html');
        },
        contentBase: path.join(__dirname__, "app"),
        hot: true,
        port: 3000,
        host: '0.0.0.0',
    },

    mode: "development",
    watch: true,
    module: {
        rules: [{
            test: /\.css$/i, //i mtlb ke capital me or small me .css ho toh usko b link karega
            use: [
                'style-loader',
                'css-loader?url=false', //url false qki url humlhudhse daalna pasand karenege
                {
                    loader: 'postcss-loader',
                    options: {
                        plugins: postCSSPlugins
                    }

                }

            ],
        }]
    }
}